%include "lib.inc"

%define QWORD_SIZE 8

global find_word
global get_value

section .text

; Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (не значения), иначе вернёт 0
; rdi - указатель на нуль-терминированную строку
; rsi - указатель на начало словаря (map.getLast())
find_word:
    push r12
    push r13
    mov r12, rdi ; key
    mov r13, rsi ; element
.loop:
    mov rdi, r12 ; key
    mov rsi, r13 ; element.next
    add rsi, QWORD_SIZE ; element.key
    call string_equals ; rdi==rsi ? rax=1 : rax=0
    test rax, rax
    jnz .ret ; key == element.key
    mov r13, [r13] ; element = element.next
    test r13, r13
    jz  .ret_err ; достигнут конец словаря (element.next == null)
    jmp .loop
.ret_err:
    xor rax, rax
    pop r13
    pop r12
    ret
.ret:
     mov rax, r13
     pop r13
     pop r12
     ret

get_value:
    add rdi, QWORD_SIZE ; element + 8 = element.key
    push rdi
    call string_length ; rax = length(element.key)
    add rax, 1 ; пропустить терминатор
    pop rdi
    add rax, rdi ; element.value
    ret




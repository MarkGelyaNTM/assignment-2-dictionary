%include "colon.inc"
%include "dict.inc"
%include "lib.inc"
%include "lib2.inc"
%include "words.inc"

%define BUFF_LENGTH 256

global _start

section .bss
    buff: resb 256

section .data
    STDIN_ERR: db "Error reading", `\n`, 0
    NOT_FOUND: db "Not found", `\n`, 0

section .text

_start:
    mov rdi, buff
    mov rsi, BUFF_LENGTH
    call read_string
    test rax, rax
    jz .stdin_err
    mov rdi, buff
    mov rsi, first_word
    call find_word
    test rax, rax
    jz .not_found
    mov rdi, rax
    call get_value ; get_value(&element): &(element.value)
    mov rdi, rax
    call print_string
    call print_newline
    xor rdi, rdi
    jmp exit
.stdin_err:
    mov rdi, STDIN_ERR
    jmp .error
.not_found:
    mov rdi, NOT_FOUND
.error:
    call print_error
    jmp exit


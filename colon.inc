; struct Element {
;   Element* next = null; // long, dq
;   String key;
;   String value;
; }

%define next 0
; key, label
%macro colon 2
%2: dq next         ; label: element.next = next
db %1, 0            ; element.key = key + '\0'
%define next %2     ; next = element
%endmacro

#!/bin/bash

keys=("key1" "key2" "key3" "key0" "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF"
"0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDE"
"0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCD")
expected_stdout=("first word explanation" "second word explanation" "third word explanation" "" "" "" "")
expected_stderr=("" "" "" "Not found" "Error reading" "Error reading" "Not found")

for i in "${!keys[@]}"; do
    key="${keys[i]}"
    expected_stdout_message="${expected_stdout[i]}"
    expected_stderr_message="${expected_stderr[i]}"

    stdout_output="$({ echo -n "$key"; } | ./main 2>/dev/null)"
    stderr_output="$({ echo -n "$key"; } | ./main 2>&1 >/dev/null)"

    if [ "$stdout_output" = "${expected_stdout_message}" ] && [ "$stderr_output" = "${expected_stderr_message}" ]; then
        echo "Test $((i+1)) passed"
    else
        echo "Test $((i+1)) failed. ./main: {stdout='$stdout_output', stderr='$stderr_output'}, expected: {'stdout'='$expected_stdout_message', stderr='$expected_stderr_message'}"
    fi
done

%include "lib.inc"

%define SYSCALL_SYS_READ 0
%define SYSCALL_SYS_WRITE 1

%define STDIN_FILENO 0
%define STDERR_FILENO 2

global read_string
global print_error

; Принимает указатель на буфер и его размер, возвращает кол-во прочитанных байт или 0 в случае ошибки
read_string:
    mov rdx, rsi ; rdx - размер буфера
    dec rdx ; место для терминатора
    mov rsi, rdi ; rsi - указатель на буфер
    mov rdi, STDIN_FILENO
    mov rax, SYSCALL_SYS_READ
    syscall
    cmp rax, 0
    jbe .ret_err ; ошибка чтения
    cmp rax, rdx
    je .ret_err ; нельзя заполнить буфер больше чем на length(buff)-1 из-за терминатора
    add rsi, rax
    inc rsi
    mov byte [rsi], `\0`
.ret:
    ret
.ret_err:
    xor rax, rax
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rdi, STDERR_FILENO
    mov rax, SYSCALL_SYS_WRITE
    syscall
    ret
